import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-page2',
  templateUrl: './page2.component.html',
  styleUrls: ['./page2.component.css']
})
export class Page2Component implements OnInit {
 

  constructor() { }

  ngOnInit(): void {
  }

  data = [
    {
      title:"Switches and Socket",
      items:[
        {itemName:"Installation - AC Switch Box",maxRate:"₹200"},
        {itemName:"Installation - AC Switch Box",maxRate:"₹200"},
        {itemName:"Installation - AC Switch Box",maxRate:"₹200"},
        {itemName:"Installation - AC Switch Box",maxRate:"₹200"},
        {itemName:"Installation - AC Switch Box",maxRate:"₹200"},
        {itemName:"Installation - AC Switch Box",maxRate:"₹200"},

      ]
    },
    {
      title:"Fan",
      items:[
        {itemName:"Installation - AC Switch Box",maxRate:"₹200"},
        {itemName:"Installation - AC Switch Box",maxRate:"₹200"},
        {itemName:"Installation - AC Switch Box",maxRate:"₹200"},
        {itemName:"Installation - AC Switch Box",maxRate:"₹200"},
        {itemName:"Installation - AC Switch Box",maxRate:"₹200"},
        {itemName:"Installation - AC Switch Box",maxRate:"₹200"},

      ]
    },
    {
      title:"Light",
      items:[
        {itemName:"Installation - AC Switch Box",maxRate:"₹200"},
        {itemName:"Installation - AC Switch Box",maxRate:"₹200"},
        {itemName:"Installation - AC Switch Box",maxRate:"₹200"},
        {itemName:"Installation - AC Switch Box",maxRate:"₹200"},
        {itemName:"Installation - AC Switch Box",maxRate:"₹200"},
        {itemName:"Installation - AC Switch Box",maxRate:"₹200"},

      ]
    },
  ]

}
