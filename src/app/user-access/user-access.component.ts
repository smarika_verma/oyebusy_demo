import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';

@Component({
  selector: 'app-user-access',
  templateUrl: './user-access.component.html',
  styleUrls: ['./user-access.component.css']
})
export class UserAccessComponent implements OnInit,AfterViewInit {
  displayedColumns: string[] = [  "user","last_signed","access_level","created_by","password","key"];
  dataSource: MatTableDataSource<any>;
  @ViewChild(MatPaginator,{static:false}) paginator: MatPaginator;
  @ViewChild(MatSort,{static:false}) sort: MatSort;



users: any[]= [
 {
  user: "priyanshi",
  email:"priyanshi2gmail.com",
  last_signed: "sep 22",
  access_level: "admin",
  created_by: "priyanshi",
  password:"123456",
  key:"som",
 },
 {
  user: "jay singh",
  email:"priyanshi2gmail.com",

  last_signed: "sep 22",
  access_level: "admin",
  created_by: "jay005",
  password:"123456",
  key:"gjgjg",
 },
 {
  user: "jhon",
  last_signed: "sep 23",
  email:"priyanshi2gmail.com",

  access_level: "admin",
  created_by: "jhon005",
  password:"111111",
  key:"gjgjg",
 },
 {
  user: "ajay",
  last_signed: "sep 23",
  email:"priyanshi2gmail.com",

  access_level: "admin",
  created_by: "jhon005",
  password:"12345",
  key:"gjgjg",
 },
 {
  user: "bhavna",
  last_signed: "sep 23",
  email:"priyanshi2gmail.com",

  access_level: "admin",
  created_by: "jhon005",
  password:"896547",
  key:'',
 },
 {
  user: "jhon",
  last_signed: "sep 23",
  email:"priyanshi2gmail.com",

  access_level: "admin",
  created_by: "jhon005",
  password:"111111",
  key:"gjgjg",
 },
 {
  user: "sapna",
  last_signed: "sep 23",
  email:"priyanshi2gmail.com",

  access_level: "admin",
  created_by: "jhon005",
  password:"111111",
  key:"gjgjg",
 }
]

  
  constructor() {
    this.dataSource = new MatTableDataSource(this.users);

   }

  ngOnInit(): void {
    
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(event: Event) {
    
    
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }


   passToggle(ev) {
   if(ev.type =='password') {
    ev.type = 'text'
     
   } else {
    ev.type = 'password'
   }
   
  }
 

  
}
