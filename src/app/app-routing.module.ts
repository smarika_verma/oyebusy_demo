import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { Page2Component } from './page2/page2.component';
import { UserAccessComponent } from './user-access/user-access.component';
import { UserAccess2Component } from './user-access2/user-access2.component';

const routes: Routes = [
{path:'home', component:HomeComponent},
{path:'page2', component:Page2Component},
{path:'user_access', component:UserAccessComponent},
{path:'user_access2', component:UserAccess2Component},




{path:'',redirectTo:'/user_access',pathMatch:'full'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
