import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { Page2Component } from './page2/page2.component';
import { UserAccessComponent } from './user-access/user-access.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialConfigModule} from '../app/core/material.config.module';
import { UserAccess2Component } from './user-access2/user-access2.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    Page2Component,
    UserAccessComponent,
    UserAccess2Component
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialConfigModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
